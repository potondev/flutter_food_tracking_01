import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FoodDetail extends StatefulWidget {
  @override
  _FoodDetailState createState() => _FoodDetailState();
}

class _FoodDetailState extends State<FoodDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 18, right: 18, top: 36),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    icon: Image.asset(
                      'assets/icon_back.png',
                      height: 20,
                      width: 20,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  IconButton(
                    icon: Image.asset(
                      'assets/icon_notification.png',
                      height: 20,
                      width: 20,
                    ),
                    onPressed: () {},
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 28, right: 28, top: 28),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Mexican",
                    style: GoogleFonts.notoSans(
                        color: Colors.black,
                        textStyle: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 26)),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    "Main Course",
                    style: GoogleFonts.notoSans(
                        color: Colors.black54,
                        textStyle: TextStyle(
                            fontWeight: FontWeight.w400, fontSize: 16)),
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 28, right: 28, top: 12),
              child: Container(
                width: double.infinity,
                child: Image.asset(
                  'assets/food_home.png',
                  height: 220,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 28, right: 28, top: 28),
              child: Container(
                height: 100,
                width: double.infinity,
                child: _GridDetail(),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 28, right: 28, top: 28),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Lorem ipsum",
                    style: GoogleFonts.notoSans(
                        color: Colors.black,
                        textStyle: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16)),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris nulla ex, varius vitae est consectetur, fringilla bibendum sem. Nam ante purus nullam.",
                    style: GoogleFonts.notoSans(
                        color: Colors.black38,
                        textStyle: TextStyle(
                            fontWeight: FontWeight.w400, fontSize: 14)),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _GridDetail extends StatelessWidget {
  Items item1 = new Items(value: '420g', label: 'Caleries');
  Items item2 = new Items(value: '56g', label: 'Protien');
  Items item3 = new Items(value: '15g', label: 'Carbo');
  Items item4 = new Items(value: '8g', label: 'Fat');
  @override
  Widget build(BuildContext context) {
    List<Items> listDetail = [item1, item2, item3, item4];
    return ListView.builder(
      itemCount: listDetail.length,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) {
        return Container(
          margin: EdgeInsets.all(4),
          width: 68,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black12, width: 1.2),
              borderRadius: BorderRadius.circular(12)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                listDetail[index].value,
                style: GoogleFonts.notoSans(
                    color: Colors.black,
                    textStyle:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 18)),
              ),
              SizedBox(
                height: 16,
              ),
              Text(
                listDetail[index].label,
                style: GoogleFonts.notoSans(
                    color: Colors.black26,
                    textStyle:
                        TextStyle(fontWeight: FontWeight.w500, fontSize: 14)),
              )
            ],
          ),
        );
      },
    );
  }
}

class Items {
  String value;
  String label;
  Items({this.value, this.label});
}
