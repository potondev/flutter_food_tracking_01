import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'foodDetail.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentIndex = 0;
  final foodImage = ['assets/food2.png', 'assets/food3.png'];
  final foodLabel = ['Lorem ipsum1', 'Lorem ipsum2'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 18, right: 18, top: 28),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      icon: Image.asset(
                        "assets/profile.png",
                        height: 40,
                        width: 40,
                      ),
                      onPressed: () {},
                    ),
                    IconButton(
                      icon: Image.asset(
                        "assets/icon_notification.png",
                        height: 40,
                        width: 40,
                      ),
                      onPressed: () {},
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 72, left: 28, right: 28),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Category",
                      style: GoogleFonts.notoSans(
                          color: Colors.black,
                          textStyle: TextStyle(
                              fontSize: 26, fontWeight: FontWeight.bold)),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      "Popular",
                      style: GoogleFonts.notoSans(
                          color: Colors.black54,
                          textStyle: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w400)),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 28),
                height: MediaQuery.of(context).size.height * 0.55,
                child: ListView.builder(
                  itemCount: foodImage.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      child: Container(
                        width: 200,
                        margin: EdgeInsets.only(left: 24),
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(foodImage[index]),
                                fit: BoxFit.cover),
                            borderRadius:
                                BorderRadius.all(Radius.circular(18))),
                        child: Align(
                          alignment: Alignment.bottomLeft,
                          child: Text(
                            foodLabel[index],
                            style: GoogleFonts.notoSans(
                                color: Colors.white,
                                textStyle: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold)),
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => FoodDetail()));
                      },
                    );
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 28, right: 28, top: 28),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Lorem ipsum",
                      style: GoogleFonts.notoSans(
                          color: Colors.black,
                          textStyle: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold)),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Text(
                      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris nulla ex",
                      style: GoogleFonts.notoSans(
                          color: Colors.black38,
                          textStyle: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w400)),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        currentIndex: _currentIndex,
        onTap: (int index) {
          setState(() {
            _currentIndex = index;
          });
        },
        showSelectedLabels: false,
        showUnselectedLabels: false,
        items: [
          BottomNavigationBarItem(
              icon: Icon(
                Icons.fastfood,
                color: _currentIndex == 0 ? Color(0xffff5604) : Colors.black,
              ),
              title: Text("")),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.list,
                color: _currentIndex == 1 ? Color(0xffff5604) : Colors.black,
              ),
              title: Text("")),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.search,
                color: _currentIndex == 2 ? Color(0xffff5604) : Colors.black,
              ),
              title: Text("")),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.add_location,
                color: _currentIndex == 3 ? Color(0xffff5604) : Colors.black,
              ),
              title: Text("")),
        ],
      ),
    );
  }
}
